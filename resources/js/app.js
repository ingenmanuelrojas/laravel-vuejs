
require('./bootstrap');

window.Vue = require('vue');


Vue.component('mis-tareas-component', require('./components/MisTareasComponent.vue').default);
Vue.component('tarea-component', require('./components/TareaComponent.vue').default);
Vue.component('form-component', require('./components/FormComponent.vue').default);

const app = new Vue({
    el: '#app',
});
